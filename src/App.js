import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

// import components
import Preloader from './components/preloader';
import Header from "./components/header";
import Skill from "./components/skills";
import Contact from './components/contact';
import Project from './components/project';

class App extends Component {
    constructor(props){
        super(props)

        this.state = {
            showPreloader: true
        }
    }

    componentDidMount(){
        setInterval(() => {this.setState({showPreloader:false})},1500)
    }
  render() {
    return (
      <div>
          {this.state.showPreloader ? (<Preloader/>) :
              (<div>
                  <Header/>
                  <Skill/>
                  <Contact/>
                  <Project/>
              </div>)}
      </div>
    );
  }
}

export default App;
