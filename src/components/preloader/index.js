import React,{Component} from 'react';
import '../../App.css'

class Preloader extends Component{
    render(){
        return(
            <div className="preloader">
                <img src="img/preloader-eclipse.gif" className=""/>
            </div>
        )
    }
}

export default Preloader;
