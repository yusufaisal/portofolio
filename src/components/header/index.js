import React,{Component} from 'react';
import Media from 'react-media';
import '../../App.css';

const video = "/video/Working-it.mp4";
class Header extends Component{
    constructor(props){
        super(props);
        this.state ={
            showNav: false
        }
    }

    btnClick = () =>{
        this.setState({
            showNav : !this.state.showNav
        })
    };

    render(){
        return(
            <header className="header" id="header">
                <article className="banner">
                    <h1 className="banner__title">my <strong className="banner__title-name">Portofolio</strong></h1>
                </article>

                <Media query="(max-width: 768px)">
                    {matches => matches ? (
                        <div className="img__container">
                            <img className="img__item" src="/img/Working-it.jpg"/>
                        </div>
                    ) : (
                        <div className="video__container">
                            <video className="video__item" id="video" autoPlay muted loop>
                                <source src={video} type="video/mp4"/>
                            </video>
                        </div>
                    )}
                </Media>

                <div className="video__overlay"></div>

                <div className="navBtn" onClick={this.btnClick}>
                    <img src="/img/butger.png"/>
                </div>
                <nav className={this.state.showNav ? "nav--show" : "nav"}>
                    <ul className="nav__links">
                        <li>
                            <a href="" id="home" className="nav__logo">
                                MYF
                            </a>
                        </li>
                        <li><a href="#home"className="nav__single-link">Home</a></li>
                        <li><a href="#contact"className="nav__single-link">Contact</a></li>
                        <li><a href="#project"className="nav__single-link">Project</a></li>
                    </ul>
                </nav>
            </header>
        )
    }
}

export default Header;