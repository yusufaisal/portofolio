import React,{Component} from  'react'
import Media from 'react-media';

class Project extends Component{

    render(){
        return(
            <section className="projects section-padding">
                <div className="reverse-section-title__container">
                    <div className="reverse-section-title__icon">
                        <img src="/img/project.png"/>
                    </div>
                    <div className="reverse-section-title__text">
                        <h1 className="reverse-section-title__name">Project</h1>
                        <h4 className="reverse-section-title__info">Here is my project</h4>
                    </div>
                </div>
                <Media query="(min-width: 768px)">
                    {matches => matches ? (
                        <div className="flexbox-parent flexbox-parent-medium">
                            <article className="project" style={{flex:  "0 0 25%"}}>
                                <img src="/img/current-projects-1.png" className="project__img"/>
                                <a href="https://open-trip32.herokuapp.com/" className="project__icon">
                                    <img src="/img/magnify.png"/>
                                </a>
                                <div className="project__outline"></div>
                            </article>
                            <article className="project" style={{flex:  "0 0 25%"}}>
                                <img src="/img/current-projects-2.jpeg" className="project__img"/>
                                <a href="/img/current-projects-2.jpeg" className="project__icon">
                                    <img src="/img/magnify.png"/>
                                </a>
                                <div className="project__outline"></div>
                            </article>
                            <article className="project" style={{flex:  "0 0 25%"}}>
                                <img src="/img/current-projects-3.jpeg" className="project__img"/>
                                <a href="/img/current-projects-3.jpeg" className="project__icon">
                                    <img src="/img/magnify.png"/>
                                </a>
                                <div className="project__outline"></div>
                            </article>
                            <article className="project" style={{flex:  "0 0 25%"}}>
                                <img src="/img/current-projects-4.jpeg" className="project__img"/>
                                <a href="/img/current-projects-4.jpeg" className="project__icon">
                                    <img src="/img/magnify.png"/>
                                </a>
                                <div className="project__outline"></div>
                            </article>
                            <article className="project" style={{flex:  "0 0 25%"}}>
                                <img src="/img/current-projects-5.jpeg" className="project__img"/>
                                <a href="/img/current-projects-5.jpeg" className="project__icon">
                                    <img src="/img/magnify.png"/>
                                </a>
                                <div className="project__outline"></div>
                            </article>
                            <article className="project" style={{flex:  "0 0 25%"}}>
                                <img src="/img/current-projects-6.jpeg" className="project__img"/>
                                <a href="/img/current-projects-6.jpeg" className="project__icon">
                                    <img src="/img/magnify.png"/>
                                </a>
                                <div className="project__outline"></div>
                            </article>
                        </div>
                    ):(
                        <div className="flexbox-parent flexbox-parent-medium">
                            <article className="project" style={{flex:  "0 0 50%"}}>
                                <img src="/img/current-projects-1.png" className="project__img"/>
                                <a href="https://open-trip32.herokuapp.com/" className="project__icon">
                                    <img src="/img/magnify.png"/>
                                </a>
                                <div className="project__outline"></div>
                            </article>
                            <article className="project" style={{flex:  "0 0 50%"}}>
                                <img src="/img/current-projects-2.jpeg" className="project__img"/>
                                <a href="/img/current-projects-2.jpeg" className="project__icon">
                                    <img src="/img/magnify.png"/>
                                </a>
                                <div className="project__outline"></div>
                            </article>
                            <article className="project" style={{flex:  "0 0 50%"}}>
                                <img src="/img/current-projects-3.jpeg" className="project__img"/>
                                <a href="/img/current-projects-3.jpeg" className="project__icon">
                                    <img src="/img/magnify.png"/>
                                </a>
                                <div className="project__outline"></div>
                            </article>
                            <article className="project" style={{flex:  "0 0 50%"}}>
                                <img src="/img/current-projects-4.jpeg" className="project__img"/>
                                <a href="/img/current-projects-4.jpeg" className="project__icon">
                                    <img src="/img/magnify.png"/>
                                </a>
                                <div className="project__outline"></div>
                            </article>
                            <article className="project" style={{flex:  "0 0 50%"}}>
                                <img src="/img/current-projects-5.jpeg" className="project__img"/>
                                <a href="/img/current-projects-5.jpeg" className="project__icon">
                                    <img src="/img/magnify.png"/>
                                </a>
                                <div className="project__outline"></div>
                            </article>
                            <article className="project" style={{flex:  "0 0 50%"}}>
                                <img src="/img/current-projects-6.jpeg" className="project__img"/>
                                <a href="/img/current-projects-6.jpeg" className="project__icon">
                                    <img src="/img/magnify.png"/>
                                </a>
                                <div className="project__outline"></div>
                            </article>
                        </div>
                        )}
                </Media>

            </section>
        )
    }
}

export default Project;