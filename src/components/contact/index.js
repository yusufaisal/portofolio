import React, {Component} from 'react';

class Contact extends Component{
    constructor(props){
        super(props);
        this.state = {

        }
    }

    render(){
        return(
            <section className="contact section-padding" id="contact">
                <div className="contact__container">
                    <h2 className="contact__title">contact me</h2>
                    <form className="contact__form">
                        <input type="email" name="" id="" className="contact__input"/>
                        <button type="submit" className="contact__submit">Submit</button>
                    </form>
                </div>
            </section>
        )
    }
}

export default Contact;