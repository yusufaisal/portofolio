import React,{Component} from 'react';
import Media from 'react-media';

class Skill extends Component{
    constructor(props){
        super(props);
        this.state = {
            skillStyle: "flexbox-parent",
        }
    }
    render(){
        return(
            <div>
                <section className="skills section-padding" id="skill">
                    <div className="section-title__container">
                        <div className="section-title__icon">
                            <img src="/img/setting.png"/>
                        </div>
                        <div className="section-title__text">
                            <h1 className="section-title__name">Skill</h1>
                            <h4 className="section-title__info">Wanna know more about me?</h4>
                        </div>
                    </div>
                    <Media query="(min-width: 768px)">
                        {matches => matches ? (
                            <div className="flexbox-parent flexbox-parent-medium">
                                <article className="skill" style={{flex: "0 0 calc(100% /3)"}}>
                                    <div className="skill__icon">
                                        <img src="/img/web.png"/>
                                    </div>
                                    <div className="skill__text">
                                        <h2 className="skill__name">Web</h2>
                                        <h4 className="skill__info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </h4>
                                    </div>
                                </article>
                                <article className="skill" style={{flex: "0 0 calc(100% /3)"}} >
                                    <div className="skill__icon">
                                        <img src="/img/design.png"/>
                                    </div>
                                    <div className="skill__text">
                                        <h2 className="skill__name">Design</h2>
                                        <h4 className="skill__info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </h4>
                                    </div>
                                </article>
                                <article className="skill" style={{flex: "0 0 calc(100% /3)"}}>
                                    <div className="skill__icon">
                                        <img src="/img/research.png"/>
                                    </div>
                                    <div className="skill__text">
                                        <h2 className="skill__name">Research</h2>
                                        <h4 className="skill__info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </h4>
                                    </div>
                                </article>
                                <article className="skill" style={{flex: "0 0 calc(100% /3)"}}>
                                    <div className="skill__icon">
                                        <img src="/img/team.png"/>
                                    </div>
                                    <div className="skill__text">
                                        <h2 className="skill__name">Team Work</h2>
                                        <h4 className="skill__info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </h4>
                                    </div>
                                </article>

                            </div>
                        ) : (
                            <div className="flexbox-parent">
                                <article className="skill">
                                    <div className="skill__icon">
                                        <img src="/img/web.png"/>
                                    </div>
                                    <div className="skill__text">
                                        <h2 className="skill__name">Web</h2>
                                        <h4 className="skill__info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </h4>
                                    </div>
                                </article>
                                <article className="skill">
                                    <div className="skill__icon">
                                        <img src="/img/design.png"/>
                                    </div>
                                    <div className="skill__text">
                                        <h2 className="skill__name">Design</h2>
                                        <h4 className="skill__info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </h4>
                                    </div>
                                </article>
                                <article className="skill">
                                    <div className="skill__icon">
                                        <img src="/img/research.png"/>
                                    </div>
                                    <div className="skill__text">
                                        <h2 className="skill__name">Research</h2>
                                        <h4 className="skill__info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </h4>
                                    </div>
                                </article>
                                <article className="skill">
                                    <div className="skill__icon">
                                        <img src="/img/team.png"/>
                                    </div>
                                    <div className="skill__text">
                                        <h2 className="skill__name">Team Work</h2>
                                        <h4 className="skill__info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </h4>
                                    </div>
                                </article>

                            </div>
                        )}
                    </Media>

                </section>
            </div>
        )
    }
}

export default Skill;